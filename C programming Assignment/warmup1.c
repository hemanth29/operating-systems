#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <time.h> 
#include <sys/stat.h>

#include "cs402.h"
#include "my402list.h"



#define MAX_LENGTH 1024
#define DESC_LENGTH 24
#define TiME_LENGTH 10
#define AMOUNT_LENGTH 10



static int balan = 0;
static int linenumber =0;
typedef struct LineDataList {
	char symbol;
	int amount_s;
	char *amount_d;
	int amt_length;
	unsigned int time_stamp;
	char desc[DESC_LENGTH];
	char time[16];
	int bal;
	
} LineData;

void processInput(int,char *a[]);
void processFile(char*);
void print(char,char*);
LineData* processLine(char*);
int processType(char *buf,char *, int i);
int processTime(char *buf, char *timestamp,int index);
int processDescription(char *buf, char *desc, int index);
My402List* processFurther(LineData *receive, My402List *list);

void printOutput(My402List *list);
void printDate(My402List *list, int index,char o);



void print(char c, char* str)
{   
	if(c == 's')
	 { 
	 	fprintf(stderr,"%s",str);
	 	exit(1);
     }
	if(c == 'p')
	   printf("%s",str);


}

void processInput(int argc,char *argv[])
{  
	
  
  if(argc < 2)
	print('s',"Less arguments to process\n");
  else if(argc > 4)
	print('s',"More arguments to process\n");
  else if(argc == 3)
  {
  	 if(strcmp(argv[1], "sort"))
  	 	print('s',"Sort command is not correct(may be arugument position");
	 processFile(argv[2]);
  }
  else if(argc == 2)
  { if(strcmp(argv[1], "sort"))
  	 	print('s',"Sort command is not correct(may be arugument position");
	processFile(NULL);
   }
  
}

void PrintTestList(My402List *pList)
{
	My402ListElem *elem=NULL;

	
	for (elem=My402ListFirst(pList); elem != NULL; elem=My402ListNext(pList, elem)) {
		LineData *ival=(LineData*)(elem->obj);

		fprintf(stdout, "%s ", ival->desc);
	}
	fprintf(stdout, "\n");
}
void BubbleForward(My402List *pList, My402ListElem **pp_elem1, My402ListElem **pp_elem2)
	/* (*pp_elem1) must be closer to First() than (*pp_elem2) */
{
	My402ListElem *elem1=(*pp_elem1), *elem2=(*pp_elem2);
	void *obj1=elem1->obj, *obj2=elem2->obj;
	My402ListElem *elem1prev=My402ListPrev(pList, elem1);
/*  My402ListElem *elem1next=My402ListNext(pList, elem1); */
/*  My402ListElem *elem2prev=My402ListPrev(pList, elem2); */
	My402ListElem *elem2next=My402ListNext(pList, elem2);

	My402ListUnlink(pList, elem1);
	My402ListUnlink(pList, elem2);
	if (elem1prev == NULL) {
		(void)My402ListPrepend(pList, obj2);
		*pp_elem1 = My402ListFirst(pList);
	} else {
		(void)My402ListInsertAfter(pList, obj2, elem1prev);
		*pp_elem1 = My402ListNext(pList, elem1prev);
	}
	if (elem2next == NULL) {
		(void)My402ListAppend(pList, obj1);
		*pp_elem2 = My402ListLast(pList);
	} else {
		(void)My402ListInsertBefore(pList, obj1, elem2next);
		*pp_elem2 = My402ListPrev(pList, elem2next);
	}
}

void BubbleSortForwardList(My402List *pList)
{
	My402ListElem *elem=NULL;
	int i=0;
	int num_items = pList->num_members;
	for (i=0; i < num_items; i++) {
		int j=0, something_swapped=FALSE;
		My402ListElem *next_elem=NULL;
	
		for (elem=My402ListFirst(pList), j=0; j < num_items-i-1; elem=next_elem, j++) {

			LineData *cur = (LineData*)(elem->obj);
			int cur_val=(int)(cur->time_stamp), next_val=0;
		   
			next_elem=My402ListNext(pList, elem);
			LineData *nxt = (LineData*)(next_elem->obj);
			next_val = (int)(nxt->time_stamp);

			if (cur_val > next_val) {
				BubbleForward(pList, &elem, &next_elem);
				something_swapped = TRUE;
			}
		}
		if (!something_swapped) break;
	}
}


My402List* processFurther(LineData *receive, My402List *list)
{		//print('s',"list is empty");
		if(My402ListEmpty(list))
		{
		 My402ListAppend(list,receive);	
		}
		else
		{
		 
		 My402ListElem *first = My402ListFirst(list);
		 while(first != NULL)
		 {  
			
			LineData *temp = (LineData*) (first->obj);
			
			if(temp->time_stamp == receive->time_stamp)
				fprintf(stderr,"Error as two data with same timestamp in line %d\n", linenumber);

			first = My402ListNext(list,first);
		 }
		My402ListAppend(list,receive);	
		}
  
  
return list;
}



 void processFile(char *argv)
{
   FILE *file;
   struct stat filename;
   stat(argv,&filename);
  if(S_ISDIR(filename.st_mode))
     print('s',"Filepath is a directory\n");

  if(argv == NULL )
  	file = stdin;
  else
	{
		file = fopen(argv,"r");
    

    }

  LineData *receive ;
  
  char buf[MAX_LENGTH+2];
  buf[MAX_LENGTH] = '\0';
  
  My402List *list = (My402List*) malloc(sizeof(My402List));
  (void)My402ListInit(list);
  

  if(file == NULL )
	 perror("File related error:");
  else
  {
	 while(fgets(buf,MAX_LENGTH+2,file))
	 {
	 	linenumber++;
		receive = (LineData*)processLine(buf);
		list = processFurther(receive,list);
        
			
	 }
	 //PrintTestList(list);
	 BubbleSortForwardList(list);
	 //printf("%s\n","After Sort" );
	 //PrintTestList(list);
	 
	 printOutput(list);


   fclose(file);
  }

}

void printOutput(My402List *list)
{
  //fprintf(stdout, "12345678901234567890123456789012345678901234567890123456789012345678901234567890\n\n\n");
  fprintf(stdout, "+-----------------+--------------------------+----------------+----------------+\n");
  fprintf(stdout, "|       Date      | Description              |         Amount |        Balance |\n");
  fprintf(stdout, "+-----------------+--------------------------+----------------+----------------+\n");
  

  int count = list->num_members;
  int i = 0;
  
   for(i = 0 ; i < count ; i++)
   {
   printDate(list,i,'d');
   printDate(list,i,'p');
   printDate(list,i,'a');
   printDate(list,i,'b');
   }
   fprintf(stdout, "+-----------------+--------------------------+----------------+----------------+\n");
  
}
void formatTime(LineData *temp)
{

		char timeInitial[25];
		time_t times;
		times = temp -> time_stamp;
		strncpy(timeInitial, ctime(&times), sizeof(timeInitial));
		char timeLater[16];
   
   int i = 0, j = 0;

  while(timeInitial[j] != '\n')
  {
	// skip the time
	if(j == 11)
	  j = 20;
	timeLater[i++] = timeInitial[j++];
  }
  timeLater[i] = '\0';
	
	printf("%s ", timeLater);


}


void formatDescription(LineData *temp)
{

   int i=0;
   while(temp->desc[i] != '\0')
	  {
		
		 i++;
	  }
	  int j =0;
  while(j<i-1 && j <24)
	  {
		
		 printf("%c",temp->desc[j]);
		 j++;
	  }
	  
	  if(j==0) 
	  	print('s',"Error in file description");

   while(j<25)
	  {
		printf("%s"," ");
		j++;
	   }

}

void formatAmount(LineData *temp, char a)
{  int i;
	if(a == 'a')
       i = temp->bal;
    else if(a== 'b')
      { 
         if(balan < 0) 
         	i = 0 - balan;
      
         else 
         	i = balan;
      }

      //printf("\nvalue of i == %d\n",i);

 int j  = 0;
 char *p = malloc(12);
 int h =0;

  while( i > 0)
  {
  	if(j == 2)
  	{
        p[j++] = '.';
        continue;
  	}
  	if(j==6)
  	{
  		p[j++] = ',';
  		continue;
  	}
  	if(j==10)
  	{
  		p[j++] = ',';
  		continue;
  	}
      h = i%10;
      i = i /10;
      p[j] = '0' + h;
     j++;

 
  }

  if(j==2) 
  	{
  		p[j++] ='.';
  	}

if(a == 'a')
{
	int tem = 13-j;

	if(temp->symbol == '+')
		printf(" ");
	else if(temp->symbol == '-' && j<=12)
		{
			printf("(");
			
		}

	while(tem-- >1)
	   printf(" ");

	if( j>12)
	{
		printf("  ?,???,???.??");
		return;
	}



	for(j=j;j>0;)
	{
	   printf("%c",p[--j]);
	}


	if(temp->symbol == '-')
		printf(")");

}


else
{

int tempor = 13-j;


while(tempor-- + ((balan > 0) ? 0 : -1 ))
   printf(" ");

  if(j>10)
	{
		printf("?,???,???.??");
		printf(")");
		return;
	}

for(j=j;j>0;)
	{
	   printf("%c",p[--j]);
	}
if(balan>0)
  printf(" ");
else
  printf(")");

}



}

void printDate(My402List *list, int index,char o)
{
	int i =0;
	My402ListElem *first = My402ListFirst(list);

 for( i = 0 ;i < index ; i++)
 {
   first = My402ListNext(list,first);
 }

    LineData *temp = (LineData*) first->obj;

	printf("%s", "| ");

 if(o == 'd')
 {    
	 formatTime(temp);
 }
 else if(o == 'p')
 {  
	formatDescription(temp);
 }
 else if(o == 'a')
 {
	formatAmount(temp,'a');
  if(temp->symbol == '+')
	 printf(" ");

	printf(" ");

 }
 else if(o == 'b')
 { 
 	
	if(temp->symbol == '+' )
	{
		balan = balan + temp->bal;
		//printf("%d",balan);

		if(balan < 0 ) printf("(");
		formatAmount(temp,'b');
	}
	else if(temp->symbol == '-' )
	{
		balan= balan - temp->bal;
	 if(balan < 0 ) printf("(");
		formatAmount(temp,'b');
	}
	else
	{
		if(balan < 0 ) printf("(");

		//printf(" ?,???,???.??");
        if(balan < 0 ) printf(")");

	}

	fprintf(stdout, " |\n");
 }


}







LineData* processLine(char * buf)
{  
  LineData *linedata = (LineData *)malloc(sizeof(LineData));
 if(buf[MAX_LENGTH] != '\0')
	print('s',"Line limit exceeded");
 else
 {   
 
   char symbol,timeStamp[TiME_LENGTH];
   char amount[AMOUNT_LENGTH];
   char *amount_s = malloc(2);
   int amt_d = 0;
   int amt_s = 0;
 
   int i = 0;
   int count = 0;
  
   if(linedata == 0 ) print('s',"Error creating a pointer of an object for saving a line");

   while(buf[i] != '\0')
   {  

	  
	   if(count == 0)
	   {
		 i = processType(buf,&symbol,i);
		 linedata->symbol = symbol;
		 count++;
	   }
	  else if(count == 1 )
	   {
		  i = processTime(buf,timeStamp,i);
		  timeStamp[10] = '\0'; 
		  i++;
		  linedata->time_stamp = atoi(timeStamp);

		  int current_time = time(NULL);
		 
		  if(linedata->time_stamp > current_time)
		  	print('s',"Date is after today and hence is wrong input\n");


		  count++;
	   }
	  else if(count == 2)
	    {    
	     memset(amount,'\0',sizeof(amount));
		 int temp = 0;
		while(buf[i] != '.')
		{
			 amount[temp++] = buf[i++];
		}
		
		amt_d = atoi(amount);
		int temp1 = 0;
		i++;

		//printf("%s    %d\n",amount,amt_d);

		while(buf[i] != '\t' )
		{  
			
			amount_s[temp1++]= buf[i++];
		}
        if(temp>7) 
        	print('s',"Error in left part of the amount as it exceeds the limit\n");
        if(temp1!=2)
        	print('s',"Error in the right part of the amount as it exceeds the limit\n");

		amt_s = atoi(amount_s);
		amt_d = amt_d;
		linedata->bal = amt_d*100 + amt_s;
		linedata->amount_s = amt_d;
		linedata->amount_d = amount_s;
        linedata->amt_length = temp;
		

		count++;

	   }

	   else if(count == 3)
	   { 
		i++;
		i =  processDescription(buf, linedata->desc, i);
	   // linedata->desc = descp;
		
	   }

   }
 }
  return linedata;
}


int processType(char *buf,char *symbol, int index)
{ 
	

  if(buf[index] != '+' && buf[index] != '-')
   {
	print('s',"Not valid type\n");
   }
   else
   {
	*symbol = buf[index];
	 index++;
	if(buf[index] != '\t')
		print('s',"Line not in correct format\n");
	 else
		index++;

   }
   
 return index;
}

int processTime(char *buf, char *timestamp,int index)
{  
	int i = 0;
   while(buf[index] != '\t')
   {   
	  

	if(buf[index] >'9' || buf[index]  < '0')
		print('s',"Error in timestamp");
	if( i >= 11 )
		print('s',"Error in timestamp length(format)");
	 timestamp[i++] = buf[index++];


   }
   return index;

}

int processDescription(char *buf, char *desc, int index)
{  
	int i = 0;
   while(buf[index] == ' ' && buf[index] != '\0' )
	 index++;
	if(buf[index] == '\0')
		print('s',"Description cannot be empty");
	else
	{

		while(buf[index] != '\0')
		{  
			if(buf[index]=='\t')
				print('s',"Too many tabs in the input");
			
			desc[i++] = buf[index++]; 
			
		}

	 

desc[i] = '\0';

	}
 return index;

}



int main(int argc,char *argv[])
{
	//printf("%d",argc);
   //printf("%s",argv[2]);

  processInput(argc,argv);


 return 0;
}




