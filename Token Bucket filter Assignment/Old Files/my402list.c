#include <stdlib.h>
#include "cs402.h"
#include "my402list.h"
#include <stdio.h>

int My402ListInit(My402List *list){
 
  (list->anchor).obj  =  NULL;
  (list->anchor).next =  &(list->anchor);
  (list->anchor).prev =  &(list->anchor);
  list->num_members = 0;
  return 1;

} 

int  My402ListEmpty(My402List *list)
{
   return list->num_members <= 0;
}


int My402ListLength(My402List *list)
{
   return list->num_members;
}

My402ListElem * My402ListFind(My402List *list, void *find)
{ 
	My402ListElem *temp = My402ListFirst(list);

	if(list->num_members <= 0) 
		return NULL;

  while(temp)
     {
        
 	  if( temp->obj == find )
           return temp;

      temp = My402ListNext(list,temp);

     } 
     return NULL;
}

My402ListElem * My402ListPrev(My402List *list, My402ListElem *present)
{   
	if(present == My402ListFirst(list))
	   return NULL;
	else
	  return present->prev;
}

My402ListElem * My402ListNext(My402List *list, My402ListElem *present)
{   
	if(present == My402ListLast(list))
	   return NULL;
	else
	  return present->next;
}

My402ListElem * My402ListLast(My402List *list)
{
	if(My402ListEmpty(list))
		return NULL;
	else
		return (list->anchor).prev;
   
 }

My402ListElem * My402ListFirst(My402List *list)
{
	if(My402ListEmpty(list))
	{
		return NULL;
	}
	else
	{
        return (list->anchor).next;
	}
}

int  My402ListInsertBefore(My402List *list, void *toInsert, My402ListElem *listElem)
{
  if(listElem == NULL)
  	return My402ListPrepend(list,toInsert);
  else 
  {
    My402ListElem  *temp =  (My402ListElem*)malloc(sizeof(My402ListElem)) ; 
    if(temp == NULL) return 0; 
    My402ListElem *prev = listElem->prev;
    temp->obj = toInsert;
    prev->next = temp;
    temp->prev = prev;
    temp->next = listElem;
    listElem->prev = temp;

    list->num_members = list->num_members + 1;
    return 1;
  }
}

int  My402ListInsertAfter(My402List *list, void *toInsert, My402ListElem *listElem)
{
   if(listElem == NULL)
   	return My402ListAppend(list,toInsert);
   else
   {
     My402ListElem *temp = (My402ListElem*)malloc(sizeof(My402ListElem));
     if(temp == NULL )return 0;
     My402ListElem *next = listElem->next;
     temp->obj = toInsert;
     listElem->next = temp;
     temp->next = next;
     next->prev = temp;
     temp->prev = listElem;

     list->num_members = list->num_members + 1 ;
     return 1;
   }
}


int  My402ListPrepend(My402List *list, void *listElem)
{
    My402ListElem *temp = (My402ListElem *)malloc(sizeof(My402ListElem));
    if(temp == NULL ) return 0;
    temp->obj = listElem;

    if(My402ListEmpty(list))
    {
       (list->anchor).next = temp;
       temp->prev = &(list->anchor);
       (list->anchor).prev = temp;
       temp->next = &(list->anchor);
       list->num_members++;
    }

    else
    { My402ListElem *first = My402ListFirst(list);
      temp->next = first;
      (list->anchor).next = temp;
      temp->prev = &(list->anchor);
      first->prev = temp;
      list->num_members++;
    }
   return 1;
}

int  My402ListAppend(My402List *list, void *listElem)
{
  
   My402ListElem *temp = (My402ListElem *) malloc(sizeof(My402ListElem));
     if(temp == NULL) return 0;
     temp->obj= listElem;

     if(My402ListEmpty(list))
     {
         (list->anchor).next = temp;
         temp->prev = &(list->anchor);
         (list->anchor).prev = temp;
         temp->next = &(list->anchor);
         list->num_members++;
     }

     else
  {   
    My402ListElem *last = My402ListLast(list);
    temp->prev = last ;
    (list->anchor).prev = temp;
    last->next = temp;
    temp->next = &(list->anchor);
    list->num_members++;
  }
  return 1;

}





void My402ListUnlink(My402List *list, My402ListElem *present)
{
   if(My402ListEmpty(list))
   	  return;
   else
   {
   	 My402ListElem *prev = present->prev;
   	 My402ListElem *next = present->next;
     prev->next = next;
     next->prev = prev;
     
     list->num_members--;
     free(present);

   }

}

void My402ListUnlinkAll(My402List *list)
{

   if(My402ListEmpty(list))
   	return;
   else
   {
       My402ListElem *present = My402ListFirst(list);
       while(present != NULL)
       {
           My402ListElem *toDel = present;
           present = My402ListNext(list,present);
           My402ListUnlink(list,toDel);
       }

     //  My402ListInit(list);
       (list -> anchor).next = &(list -> anchor);
       (list -> anchor).prev = &(list -> anchor);
   
   }

}

