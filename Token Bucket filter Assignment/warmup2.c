    #include <stdio.h>
    #include <string.h>
    #include <pthread.h>
    #include <sys/time.h>
    #include <signal.h>
    #include <unistd.h>
    #include <stdlib.h>
    #include <math.h>
    #include <stdbool.h>
    #include <sys/stat.h>

    #include "cs402.h"
    #include "my402list.h"


    #define INPUT_PARAMETERS_NUM 7
    #define MAX_LENGTH 1024
    #define AT_ONCE_n 100000

    /* ------------------------------------------------------------------------Global Variables-----------------------------------------------------------------------------   */
    enum inputFromCommand
    {
      LAMBDA_V,
      MU_V,
      R_V,
      B_V,
      P_V,
      NUM_V,
      TSFILE_V
    };



    typedef struct packetInfo
    {
     double packetRate;
     double serverRate;
     int numOfTokens;

     int id;

     double q1Arr;
     double q2Arr;
     double s1Arr;
     double s2Arr;

     double q1Dep;
     double q2Dep;
     double s1Dep;
     double s2Dep;

    } packet;

    /*-------------------------------------------------------------------------/Global Variables ------------------------------------------------------------------------------   */


    char *initialValues[INPUT_PARAMETERS_NUM];
    bool R_V_SET = false , B_V_SET = false, LAMBDA_V_SET = false, MU_V_SET =false , P_V_SET = false , NUM_V_SET = false ;
    My402List q1Queue, q2Queue, initPackList, Collect_List;
    int numofPacketsTotal = 0;
    bool isFilePresent = false;
    double tokenRatefinal = 0, bucketSize = 0;
    struct timeval begining_time;
    double begin_time;
    int presentTokens = 0;
    long int lineCount = 0;
    pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
    pthread_t p_id, t_id, s1_id, s2_id,ctrl_id ;
    long int limitExceedPacketVal = 0;
    FILE * file;
    pthread_cond_t q2EmptyCond;
    int servicedPacketsCount = 0;
    packet *printPacket;
    bool y = true;
    float average_arrival_time = 0;
    float average_service_time = 0;
    float average_no_packets_q1 = 0;
    float average_no_packets_q2 = 0;
    float average_no_packets_s1 = 0;
    float average_no_packets_s2 = 0;
    float average_system_time = 0;
    float standard_dev_sys  = 0;
    float arrivedTokens=0;
    float droppedTokens=0;
    float arrivedPackets=0;
    float droppedPackets=0;
    float timeInSys_eachPack_square =0;
    sigset_t set;
    bool server_stop = false;
    /*-------------------------------------------------------------------------Function Declarations ------------------------------------------------------------------------------   */

    void initializeValues();
    void checkInput(int argc, char *argv[]);
    void processLine(char *buf);
    void pickFromFileAndSave();
    void prepareForDetModeAndSave();
    void printPackets();
    void startThreads ();
    void * packetThread(void *inp);
    void * tokenThread(void *inp);
    void * server1Thread(void *inp);
    packet * preparePacket();
    void * packetThreadLargeNormal(void *inp);
    void * server2Thread(void *inp);
    void * controlThread(void *inp);

    /*---------------------------------------------------------------------Utility Functions----------------------------------------------------------------------------------------*/

    void outputError(char *str)
    {
      fprintf(stderr,"OutputError: %s \n",str);
      exit(0);
    }

    void initializeValues()
    {
      int i = 0;
      for(i = 0; i < INPUT_PARAMETERS_NUM; i++)
        initialValues[i] = NULL;

      memset(&q1Queue, 0, sizeof(My402List));
      memset(&q2Queue, 0, sizeof(My402List));
      (void)My402ListInit(&q2Queue);
      (void)My402ListInit(&q1Queue);
    }

    double convertToMsec(struct timeval timeGiven)
    {
      double returnTime = 1000 * (timeGiven.tv_sec - begining_time.tv_sec) + ((double)(timeGiven.tv_usec - begining_time.tv_usec)) / 1000;
      return returnTime;
    }

    void printStats()
    {

     struct timeval present_time;
     double presentTime;
     gettimeofday(&present_time,0);
     presentTime = convertToMsec(present_time);
     presentTime = presentTime - begin_time;
printf("\n\n-----------------------------Stats-------------------------------------");
     if((arrivedPackets+droppedPackets) == 0)
      printf("\n\naverage arrival time           : %s\n", "N/A as Totak packets into Q1 is zero");
    else
      printf("\n\naverage arrival time           : %.6g \n", (float)((average_arrival_time/(arrivedPackets+droppedPackets))/1000));


    if(servicedPacketsCount == 0)
      printf("average packet servicetime     : %s\n", "N/A as serviced Packets in servers is zero");
    else
      printf("average packet servicetime     : %.6g \n\n", (float)((average_service_time/servicedPacketsCount)/1000));


    printf("average time in q1             : %.6g \n", (float)((average_no_packets_q1/presentTime)));
    printf("average time in q2             : %.6g \n", (float)((average_no_packets_q2/presentTime)));
    printf("average time in s1             : %.6g \n", (float)((average_no_packets_s1/presentTime)));
    printf("average time in s2             : %.6g \n\n",(float)((average_no_packets_s2/presentTime)));

    if(servicedPacketsCount == 0) 
    {
      printf("average System time            : %s \n", "N/A as serviced Packets in servers is zero" );      
    }
    else
    {
      printf("average System time            : %.6g \n", (float)((average_system_time/servicedPacketsCount)/1000));      
    }
     if(servicedPacketsCount <= 1) 
    {     
      printf("Standard Deviation             : %s \n\n", "N/A as serviced Packets in servers is zero or 1(cannot calculate(0/0)" );
    }
    else
    { 
      printf("Standard Deviation             : %.6g \n\n", (sqrt((timeInSys_eachPack_square/servicedPacketsCount) - (((average_system_time/servicedPacketsCount))*((average_system_time/servicedPacketsCount)))))/1000);
    }

    if(arrivedPackets+droppedPackets == 0)
    {
      printf("Packets:Dropped: 0 Arrived to q1 : 0 \nProb: N/A as no packets arrived in the system\n");
    }
    else
    {
      printf("Packets:Dropped: %.6g Arrived to q1 : %.6g \nProb : %.6g \n", droppedPackets ,arrivedPackets, (float)(droppedPackets/(arrivedPackets+droppedPackets) ));
    }
    if(arrivedTokens+droppedTokens == 0)
    {
      printf("Tokens: Dropped: 0 Arrived : 0 \nProb : N/A as no tokens arrived in the system\n");
    }
    else
    {
      printf("Tokens: Dropped: %.6g Arrived :%.6g \nProb : %.6g \n", droppedTokens, arrivedTokens, (float)(droppedTokens/(arrivedTokens+droppedTokens)));
    }

 printf("-----------------------------Stats-------------------------------------\n\n");
  //     printf("\nServiced Packets at server  : %d\n\n",servicedPacketsCount);

      // printf("total packets = %lf \n",(arrivedPackets));
  //    // printf("total packets = %lf \n",(average_arrival_time));
  // printf("-----------------------------Additional Stats--------------------------------");
    }


    bool checkDigit(char *str)
    {
      int i = 0;
      int dotCount = 0;
      while(str[i] != '\0' && str[i] != '\n')
      { 

       if( (str[i] >='0' && str[i] <= '9' ) || (str[i] =='.' && dotCount==0 ) )
       {
         if(str[i]=='.') 
          dotCount =1;
          i++;
          continue;
       }

       else
       {
        return false;
       } 

    }
     return true;
    }

    void cleanQueues()
    {  

     pthread_cancel(p_id);
     pthread_cancel(t_id);
     server_stop = true;

     pthread_mutex_lock(&mutex);

     while(!My402ListEmpty(&q1Queue))
    {
       My402ListElem *element = My402ListFirst(&q1Queue);
       packet *presentPacket = element->obj;
       My402ListUnlink(&q1Queue,element);
       printf("Packet %d dropped from Q1 because cntrl c was pressed\n",presentPacket->id);

    }
     while(!My402ListEmpty(&q2Queue))
    {
       My402ListElem *element = My402ListFirst(&q2Queue);
       packet *presentPacket = element->obj;
       My402ListUnlink(&q2Queue,element);
       printf("Packet %d dropped from Q2 because cntrl c was pressed\n",presentPacket->id);

    }
  

     pthread_cond_broadcast(&q2EmptyCond);
     pthread_mutex_unlock(&mutex);

    }
    void * controlThread(void *inp)
    {   
      int temp;
      sigwait(&set,&temp);
      cleanQueues(); 

      return(0);

    }

    /*------------------------------------------------------------------------------Main Function--------------------------------------------------------------------------------------------------*/

    int main(int argc, char *argv[])
    {  

     sigemptyset(&set);
     sigaddset(&set,SIGINT);


     sigprocmask(SIG_BLOCK,&set,0);
     
     initializeValues();
     checkInput(argc,argv);

     if(isFilePresent == true)
      pickFromFileAndSave();

      /*-------------------------------------------------------------------------Emulation parameters------------------------------------------------------------------------------------------- */  

    printf("\n\nEmulation Parameters\n");

    printf("number to arrive = %d\n",numofPacketsTotal);
    printf("lamdda = %s          \n",initialValues[LAMBDA_V]);
    printf("mu = %s              \n",initialValues[MU_V]);
    printf("r = %s               \n",initialValues[R_V]);
    printf("B = %.0lf            \n",bucketSize);
    printf("P = %s               \n",initialValues[P_V]);
    printf("tsfile = %s         \n\n",initialValues[TSFILE_V]);

    startThreads();
    printStats();
   
    exit(0);
    }
    /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    void checkInput(int argc, char *argv[])
    {
      int i = 0;
      if( argc%2 == 0) 
      	outputError("Missing value or Incorrect command: Malformed Command");
      for(i = 1 ; i < argc ; i++)
      {


       if(!strcmp(argv[i],"-lambda"))
       {
        initialValues[LAMBDA_V] = argv[++i];
        if(checkDigit(initialValues[LAMBDA_V]) == false)
         outputError("Invalid value entered for lambda(Malformed Command)");
     }
     else if(!strcmp(argv[i],"-mu"))
     {
      initialValues[MU_V] = argv[++i]+'\0';
      if(checkDigit(initialValues[MU_V]) == false)
       outputError("Invalid value entered for MU (Malformed Command)");
    }
    else if(!strcmp(argv[i],"-r"))
    {
      initialValues[R_V] = argv[++i];
      tokenRatefinal = (1 / atof(initialValues[R_V])) * 1000 ;
      if(checkDigit(initialValues[R_V]) == false)
       outputError("Invalid value entered for R (Malformed Command)");
     if(tokenRatefinal>10000)
       tokenRatefinal = 10000;

    }
    else if(!strcmp(argv[i],"-B"))
    {
      initialValues[B_V] = argv[++i];
      bucketSize = atoi(initialValues[B_V]);
      if(checkDigit(initialValues[B_V]) == false)
       outputError("Invalid value entered for B (Malformed Command)");

    }
    else if(!strcmp(argv[i],"-P"))
    { 
      initialValues[P_V] = argv[++i];
      if(checkDigit(initialValues[P_V]) == false)
       outputError("Invalid value entered for P (Malformed Command)");
    }
    else if(!strcmp(argv[i],"-n"))
    {
      initialValues[NUM_V] = argv[++i];
      numofPacketsTotal = atoi(initialValues[NUM_V]);
      if(checkDigit(initialValues[NUM_V]) == false)
       outputError("Invalid value entered for n (Malformed Command)");

    }
    else if(!strcmp(argv[i],"-t"))
    {
      initialValues[TSFILE_V] = argv[++i];
      isFilePresent = true;
    }
    else
      outputError("Malformed Command");

    }

    if(initialValues[R_V] == NULL)
    { 
      tokenRatefinal = (1/1.5)*1000 ;
      initialValues[R_V] = "1.5\0";
      R_V_SET = true;
    }

    if(initialValues[B_V] == NULL)
    {
      bucketSize = 10;
      initialValues[B_V] = "10\0";
      B_V_SET = true;
    }

    if(isFilePresent == false && initialValues[LAMBDA_V] == NULL)
    {
      initialValues[LAMBDA_V] = "1\0";
      LAMBDA_V_SET = true;

    }
    if(isFilePresent == false && initialValues[MU_V] == NULL)
    {
      initialValues[MU_V] = "2.857\0";
      MU_V_SET = true;

    }
    if(isFilePresent == false && initialValues[NUM_V] == NULL)
    {     
      numofPacketsTotal = 20;
      initialValues[NUM_V] = "20\0";
      NUM_V_SET = true;

    }
    if(isFilePresent == false && initialValues[P_V] == NULL)
    {
      initialValues[P_V] = "3\0";
      P_V_SET = true;

    } 
    }


    void pickFromFileAndSave()
    {
        struct stat filename;
        stat(initialValues[TSFILE_V],&filename);
        if(S_ISDIR(filename.st_mode))
            outputError("Filepath is a directory\n");

        char buf[MAX_LENGTH + 2];
        buf[MAX_LENGTH] = '\0';


     if((file = fopen(initialValues[TSFILE_V], "r")) == NULL)
      {  
           perror("File related error:");
           exit(1);
      }


     if(fgets(buf, MAX_LENGTH+2, file))
       {

          if(buf[MAX_LENGTH] != 0)
            outputError("First Line is not 1024 characters length");

          initialValues[NUM_V] = buf;
               
          if(checkDigit(initialValues[NUM_V]) == false)
            outputError("invalid value in line 1 file for n");

          numofPacketsTotal = atoi( initialValues[NUM_V]);

       }

    }

    void startThreads ()
    {



      gettimeofday(&begining_time,0);
      begin_time = convertToMsec(begining_time);

      printf("\n%012.3f ms: Start of Emulation \n\n",begin_time-begin_time);

      if(pthread_create(&p_id, 0, packetThreadLargeNormal, 0))
         outputError("Packet long Thread creation failed");

      if(pthread_create(&t_id, 0, tokenThread, 0))
        outputError("Token Thread creation failed");

      if(pthread_create(&s1_id, 0, server1Thread, 0))
        outputError("Server 1 Thread creation failed");

      if(pthread_create(&s2_id, 0, server2Thread, 0))
        outputError("Server 2 Thread creation failed");

      if(pthread_create(&ctrl_id, 0, controlThread, 0))
        outputError("Server 2 Thread creation failed");


    pthread_join(p_id, 0);
    pthread_join(t_id, 0);
    pthread_join(s1_id, 0);
    pthread_join(s2_id, 0);

    }

    void * tokenThread(void *inp)
    {  

      struct timeval present_time;

      double prev_tok_time = begin_time;
      double presentTime = 0;
      double sleepTime;
      int tokenId = 0;
      while(1)
      {
        pthread_cleanup_push(pthread_mutex_unlock,&mutex);
        pthread_mutex_lock(&mutex);
        pthread_cleanup_pop(0);


        if ( ((droppedPackets+limitExceedPacketVal) >= numofPacketsTotal ))
        { 
          pthread_mutex_unlock(&mutex);
          break;
        }


        tokenId++;
        gettimeofday(&present_time,0);
        presentTime = convertToMsec(present_time);


        sleepTime = tokenRatefinal - (presentTime - prev_tok_time);
        pthread_mutex_unlock(&mutex);

        if(sleepTime < 0)
          sleepTime = 0;

        usleep(sleepTime*1000);
        gettimeofday(&present_time,0);
        presentTime = convertToMsec(present_time);

        pthread_cleanup_push(pthread_mutex_unlock,&mutex);
        pthread_mutex_lock(&mutex);
        pthread_cleanup_pop(0);

        presentTokens++;
    /*-------------------------------------------------------------------------------------------------------------------*/
        if ( ((droppedPackets+limitExceedPacketVal) >= numofPacketsTotal ))
        { 
          pthread_mutex_unlock(&mutex);
          break;
        }    
    /*-------------------------------------------------------------------------------------------------------------------*/
        if(presentTokens > bucketSize)
        {

          printf("%012.3lf: token  %d arrives,dropped\n" , presentTime,tokenId);
          presentTokens = bucketSize;
          prev_tok_time = presentTime;
          droppedTokens++;

        }
        else
        {

          gettimeofday(&present_time,0);
          presentTime = convertToMsec(present_time);

          printf("%012.3lf: token  %d arrives,token bucket has %d tokens\n" , presentTime,tokenId,presentTokens);
          prev_tok_time = presentTime;

          arrivedTokens++;

        }



        if(My402ListLength(&q1Queue) >= 1 )
        {
          My402ListElem *toRemove = My402ListFirst(&q1Queue);
          packet *Temppacket = toRemove->obj;

          if(Temppacket->numOfTokens <= presentTokens && presentTokens!=0)
          {
            limitExceedPacketVal++;

            presentTokens = presentTokens - Temppacket->numOfTokens;

            (void)My402ListUnlink(&q1Queue, toRemove);

            gettimeofday(&present_time,0);
            presentTime = convertToMsec(present_time);

            Temppacket->q1Dep = presentTime;  

            printf("%012.3lf: packet %d leave q1,time in q1=%.3lfms,tokenbucket= %d token\n" , Temppacket->q1Dep, Temppacket->id,(Temppacket->q1Dep-Temppacket->q1Arr),presentTokens);   
            gettimeofday(&present_time,0);
            presentTime = convertToMsec(present_time);
            Temppacket->q2Arr = presentTime;
            bool q2Empty = My402ListEmpty(&q2Queue);
            printf("%012.3lf: packet %d enters q2 \n" , Temppacket->q2Arr,Temppacket->id);
            My402ListAppend(&q2Queue,Temppacket);

            if(q2Empty)
              pthread_cond_signal(&q2EmptyCond);



          }
        }
        pthread_mutex_unlock(&mutex);
      }

      return(0);
    }


    void * packetThreadLargeNormal(void *inp)
    {



     packet *presentPacket;


     struct timeval present_time;
     double presentTime;
     double timeToSleep;
     double prev_pack_time = begin_time;
     int count = 0;


     while(1)
     {  
      pthread_cleanup_push(pthread_mutex_unlock,&mutex);
      pthread_mutex_lock(&mutex);
      pthread_cleanup_pop(0);

      if((arrivedPackets+droppedPackets) == numofPacketsTotal )
      {
       pthread_mutex_unlock(&mutex);
       break;
     }

     else
     {

      presentPacket = preparePacket();

    }

    pthread_mutex_unlock(&mutex);

    count++;
    presentPacket->id = count;

    gettimeofday(&present_time,0);
    presentTime = convertToMsec(present_time);

    timeToSleep = presentPacket->packetRate - (presentTime - prev_pack_time);

    if(timeToSleep < 0)
      timeToSleep = 0;

    usleep(timeToSleep*1000);

    pthread_cleanup_push(pthread_mutex_unlock,&mutex);
    pthread_mutex_lock(&mutex);
    pthread_cleanup_pop(0);

    gettimeofday(&present_time,0);
    presentTime = convertToMsec(present_time);

    if( presentPacket->numOfTokens > bucketSize)
    {     
     fprintf(stdout, "%012.3lf: packet %d arrives, needs %d tokens,requesting time = %.3lfms,dropped\n" , presentTime, presentPacket->id,presentPacket->numOfTokens,(presentTime- prev_pack_time));
     average_arrival_time += (double)(presentTime- prev_pack_time);
     pthread_mutex_unlock(&mutex);
     droppedPackets++;
     prev_pack_time = presentTime;
     free(presentPacket);
     continue;
    }

    else
     {  arrivedPackets++;
      printf("%012.3lf: packet %d arrives, needs %d tokens,requesting time = %.3lfms,\n" , presentTime, presentPacket->id, presentPacket->numOfTokens,(presentTime- prev_pack_time) );
    }

    (void)My402ListAppend(&q1Queue,presentPacket);

    gettimeofday(&present_time,0);
    presentTime = convertToMsec(present_time);
    presentPacket->q1Arr = presentTime;

    average_arrival_time += (double)(presentTime- prev_pack_time);
    printf("%012.3lf: packet %d enters q1,\n" , presentTime, presentPacket->id);
    prev_pack_time = presentTime;


    pthread_mutex_unlock(&mutex);



    }
    pthread_cleanup_push(pthread_mutex_unlock,&mutex);
    pthread_mutex_lock(&mutex);
    pthread_cleanup_pop(0);
    pthread_cond_broadcast(&q2EmptyCond);
    pthread_mutex_unlock(&mutex);

    return(0);
    }



    packet * preparePacket()
    {


      packet *eachPacket;


      if(isFilePresent == false)
      {

       pthread_cleanup_push(free,eachPacket);

       eachPacket = (packet *)malloc(sizeof(packet));

       pthread_cleanup_pop(0);

       if(LAMBDA_V_SET == true)
        eachPacket->packetRate = (1 * 1000);
      else
      {  
        eachPacket->packetRate = (1 / atof(initialValues[LAMBDA_V]) ) * 1000 ;
        if(eachPacket->packetRate > 10000)
          eachPacket->packetRate = 10000;
      }

      if(MU_V_SET == true)
        eachPacket->serverRate = ( 2857.788);
      else
      {
        eachPacket->serverRate =  ( 1 / atof(initialValues[MU_V]) ) * 1000 ;
        if(eachPacket->serverRate > 10000)
          eachPacket->serverRate = 10000;
      }


      if(P_V_SET == true)
        eachPacket->numOfTokens = 3;
      else
        eachPacket->numOfTokens =   atof(initialValues[P_V]);


    }

    else
    {

     pthread_cleanup_push(free,eachPacket);

     eachPacket = (packet *)malloc(sizeof(packet));

     pthread_cleanup_pop(0);

     char buf[MAX_LENGTH + 2];
     buf[MAX_LENGTH] = '\0';

     int i, index = -1;
     char var[3][MAX_LENGTH];

     i = 0, index = -1;

     if(fgets(buf, MAX_LENGTH + 2, file))
     {

      while(buf[i] != '\0' && buf[i] != '\n')
      {
        if(buf[i] == ' ' || buf[i] == '\t')
        {
          i++;
          continue;
        }
        index++;
        if(index >= 3)
          break;

        int u = 0;
        while(buf[i] != ' ' && buf[i] != '\t' && buf[i] != '\0')
        {
          var[index][u++] = buf[i];
          i++;
        }
        var[index][u] = '\0';
      }

      lineCount++;            

      initialValues[LAMBDA_V] = var[0] ;
      initialValues[P_V] = var[1];
      initialValues[MU_V] = var[2];

      if(checkDigit(initialValues[LAMBDA_V]) == false)
        {fprintf(stderr,"Invalid value presnt in line %ld file for lambda\n\n",lineCount);
          exit(0);
          }
      if(checkDigit(initialValues[P_V]) == false)
      {
        
        fprintf(stderr,"Invalid value presnt in line %ld file for P\n\n",lineCount);
        exit(0);
      }
      if(checkDigit(var[2]) == false)
      {
        fprintf(stderr,"Invalid value presnt in line %ld file for mu\n\n",lineCount);
        exit(0);
      }


      eachPacket->packetRate = atof(initialValues[LAMBDA_V]);
      eachPacket->numOfTokens = atoi(initialValues[P_V]);
      eachPacket->serverRate = atof(initialValues[MU_V]);

      return eachPacket;

    }
    }

    return eachPacket;
    }


    void * server1Thread(void *inp)
    {
      struct timeval present_time;
      double serviceWait = 0;
      double serviceTime = 0;

      double presentTime =0;

      while(1)
      {  
       pthread_mutex_lock(&mutex);

       if( (((arrivedPackets+droppedPackets) == numofPacketsTotal) && (servicedPacketsCount == arrivedPackets)) || server_stop == true )
       {

        pthread_mutex_unlock(&mutex);
        break;
      } 

      while(My402ListEmpty(&q2Queue) && server_stop == false)
      { 
       if((((arrivedPackets+droppedPackets) == numofPacketsTotal) && (servicedPacketsCount == arrivedPackets))|| server_stop == true)
       {
         pthread_mutex_unlock(&mutex);
         break;
       } 


       pthread_cond_wait(&q2EmptyCond, &mutex);

     }
                 // printf("\n\nEnterererereerer\n\n");
     if(!My402ListEmpty(&q2Queue))
     {
       My402ListElem *element = My402ListFirst(&q2Queue);
       packet *presentPacket = element->obj;

       gettimeofday(&present_time,0);
       presentTime = convertToMsec(present_time);
       presentPacket->q2Dep = presentTime;

       printf("%012.3lf: packet %d leaves q2,time in q2 = %.3lf \n",presentTime,presentPacket->id,(presentPacket->q2Dep - presentPacket->q2Arr));
       (void)My402ListUnlink(&q2Queue,element);

       printf("%012.3lf: packet %d starts at server1 requesting %.3lf servicetime \n",presentTime,presentPacket->id,presentPacket->serverRate);

       serviceWait =  presentPacket->serverRate;

       pthread_mutex_unlock(&mutex);
       usleep(serviceWait*1000);
       pthread_mutex_lock(&mutex);


       gettimeofday(&present_time,0);
       presentTime = convertToMsec(present_time);

       serviceTime = presentTime - presentPacket->q2Dep;
       average_service_time += serviceTime;
       average_system_time += (presentTime - presentPacket->q1Arr);
       timeInSys_eachPack_square += (presentTime - presentPacket->q1Arr) * (presentTime - presentPacket->q1Arr) ;

       average_no_packets_s1 += serviceTime;

       average_no_packets_q1 += (double)(presentPacket->q1Dep-presentPacket->q1Arr);
       average_no_packets_q2 += (presentPacket->q2Dep - presentPacket->q2Arr);
       printf("%012.3lf: packet %d,departs s1,servicetime = %.3lf,TotalTime = %.3lf \n",presentTime,presentPacket->id,serviceTime,(presentTime - presentPacket->q1Arr) );

       servicedPacketsCount++;

     }  


     pthread_mutex_unlock(&mutex);

    }

    pthread_mutex_lock(&mutex);

    pthread_cond_broadcast(&q2EmptyCond);

    pthread_mutex_unlock(&mutex);

    printf("\nExited s1\n");
    return(0);
    }


    void * server2Thread(void *inp)
    {
      struct timeval present_time;
      double serviceWait = 0;
      double serviceTime = 0;

      double presentTime =0;

      while(1)
      {   

       pthread_mutex_lock(&mutex);

       if( (((arrivedPackets+droppedPackets) == numofPacketsTotal) && (servicedPacketsCount == arrivedPackets)) || server_stop == true )
       {

        pthread_mutex_unlock(&mutex);
        break;
      } 

      while(My402ListEmpty(&q2Queue) && server_stop == false)
      { 
       if((((arrivedPackets+droppedPackets) == numofPacketsTotal) && (servicedPacketsCount == arrivedPackets))|| server_stop == true)
       {
         pthread_mutex_unlock(&mutex);
         break;
       } 


       pthread_cond_wait(&q2EmptyCond, &mutex);

     }

                           //  printf("\n\nEnterererereerer\n\n");
     if(!My402ListEmpty(&q2Queue))
     {
      My402ListElem *element = My402ListFirst(&q2Queue);
      packet *presentPacket = element->obj;

      gettimeofday(&present_time,0);
      presentTime = convertToMsec(present_time);
      presentPacket->q2Dep = presentTime;


      printf("%012.3lf: packet %d leaves q2,time in q2 = %.3lf \n",presentTime,presentPacket->id,(presentPacket->q2Dep - presentPacket->q2Arr));
      (void)My402ListUnlink(&q2Queue,element);


      printf("%012.3lf: packet %d starts at server2 requesting %.3lf servicetime \n",presentTime,presentPacket->id,presentPacket->serverRate);

      serviceWait =  presentPacket->serverRate;

      pthread_mutex_unlock(&mutex);
      usleep(serviceWait*1000);
      pthread_mutex_lock(&mutex);

      gettimeofday(&present_time,0);
      presentTime = convertToMsec(present_time);

      serviceTime = presentTime - presentPacket->q2Dep;
      average_service_time += serviceTime;
      average_system_time += (presentTime - presentPacket->q1Arr);
      timeInSys_eachPack_square += (presentTime - presentPacket->q1Arr) * (presentTime - presentPacket->q1Arr) ;
      average_no_packets_s2 += serviceTime;
      average_no_packets_q1 += (double)(presentPacket->q1Dep-presentPacket->q1Arr);
      average_no_packets_q2 += (presentPacket->q2Dep - presentPacket->q2Arr);

      printf("%012.3lf: packet %d,departs s2,servicetime = %.3lf,TotalTime = %.3lf \n",presentTime,presentPacket->id,serviceTime,(presentTime - presentPacket->q1Arr) );

      servicedPacketsCount++;

    }

    pthread_mutex_unlock(&mutex);
    } 


    pthread_mutex_lock(&mutex);

    pthread_cond_broadcast(&q2EmptyCond);

    pthread_mutex_unlock(&mutex);

    printf("\nExited s2\n\n");
    return(0);
    }













